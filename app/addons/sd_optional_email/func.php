<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_generate_uniq_email() 
{
    $email = false;

    $addon_info = Registry::get('addons.sd_optional_email');
    if (!empty($addon_info['email_prefix']) && !empty( $addon_info['email_suffix'])) {
        $id =  TIME . mt_rand(0, TIME);
        $email = $addon_info['email_prefix'] .$id . $addon_info['email_suffix'];
    }

    return $email;
}