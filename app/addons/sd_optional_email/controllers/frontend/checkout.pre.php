<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (empty($REQUEST['user_data']['email'])) {
        $_REQUEST['user_data']['email'] = fn_generate_uniq_email();
    }
}