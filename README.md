# Module: optional email #

The email is not required for registration.

### In detail ###

Designed for cs cart

### How to install and configure the module? ###

To install this module, copy all the files to / cs-cart / with the folder structure preserved.
Install the module on the "Modules" page.
